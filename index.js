/*mini activity
Display the following student number in our console.

2020-1923
2020-1924
2020-1925
2020-1926
2020-1927
*/

let batch1 = "2020-1923"
console.log(batch1)
let batch2 = "2020-1924"
console.log(batch2)
let batch3 = "2020-1925"
console.log(batch3)
let batch4 = "2020-1926"
console.log(batch4)
let batch5 = "2020-1927"
console.log(batch5)

let batchNumber = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"]

/*
	Arrays are used to store multiple related values in single variable.
	-declared using square brackets ([]) also known as "Array Literals".

	Syntax:
		let/const arrayName = [elementA, elementB ... elementN]


*/

let grades = [98.5, 94.3, 89.2, 90.1]
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"]

// not recommended
let mixedArr = [12, "Asus", null, undefined, {}];
console.log(mixedArr)

// each element of an array can also be written in separate lines
let myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake sass"
];
console.log(myTasks)


// we can also store values of separate variables in an array
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
console.log(cities)

// Array Length Property
// can delete specific item in an array

/*let theBeatles = ["John", "Paul", "Georgie", "Ringo"]
theBeatles.length++
console.log(theBeatles)*/

let blankArr = [];
console.log(blankArr.length)  //result is 0 bec array is empty

let fullName = "Jamie Noble" 
console.log(fullName.length) //elements are counted if you use .length in array

// deleting an element in an array
myTasks.length = myTasks.length -1
console.log(myTasks.length)
console.log(myTasks)


cities.length--
console.log(cities)

// can we delete a character in a string using .length property? No, it doesnt work

console.log(fullName.length)
fullName.length = fullName.length - 1
console.log(fullName.length)
fullName.length--
console.log(fullName)

let theBeatles = ["John", "Paul", "Georgie", "Ringo"]
theBeatles.length++
console.log(theBeatles)
// to add an element, array[i] = "value"
theBeatles[4] = "Jenkins"
console.log(theBeatles)

/*
accessing elements of an array

	Syntax:
		arrayname[index]
*/

console.log(grades[0]);
console.log(computerBrands[3]);
console.log(grades[20]); //undefined if the index element does not exist

let lakersLegends =["kobe", "shaq", "lebron", "magic", "kareem"]
console.log(lakersLegends[1])
console.log(lakersLegends[3])

//can we save an array item/s in another variable
let currentLaker = (lakersLegends[2])
console.log(currentLaker)

//replacing an element
console.log("Arrays before reassignment")
console.log(lakersLegends)
lakersLegends[2] = "Pau Gasol"
console.log("Arrays after reassignment")
console.log(lakersLegends)


// accessing the last element of an array
let bullsLegend =["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"]
let lastElementIndex = bullsLegend.length - 1
console.log(bullsLegend[lastElementIndex])

// adding items into the array
let newArr = []
console.log(newArr);
console.log(newArr[0]);
newArr[0] = "Jennie";
console.log(newArr);
newArr[1] = "Jisoo";
console.log(newArr);
newArr.length++
newArr[newArr.length-1] = 'Lisa';
console.log(newArr)


//Looping over an Array

for(let index = 0; index < newArr.length; index++){

	console.log(newArr[index])
}

let numArr = [5, 12, 30, 46, 50, 88]
for(let index = 0; index < numArr.length; index++){
	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5")
	}
	else{
		console.log(numArr[index] + " is not divisible by 5")
	}
}

//Multidimensional Array
let chessBoard = [
["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
];

console.log(chessBoard[1][4]);  //prints e2
console.log("Pawn motes to: " + chessBoard[7][4])


